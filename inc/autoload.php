<?php
    //Theme Setup
    get_template_part('inc/theme/mobile-detect');
    get_template_part('inc/theme/functions');
    get_template_part('inc/theme/custom-fields');
    get_template_part('inc/theme/post-types');
    get_template_part('inc/theme/scripts');
    get_template_part('inc/theme/features');
    get_template_part('inc/theme/image-size');
    get_template_part('inc/theme/bootstrap-menu');
    get_template_part('inc/theme/menu');
    get_template_part('inc/theme/sidebar');
    get_template_part('inc/theme/ajax');
    get_template_part('inc/theme/loader');
?>