<?php
    //Image size
    $sizes = array(
        array('header-slider', 1360, 600, true),
        array('header-slider-mobile', 1000, 400, true),
        array('home-thumb', 500, 500, true)
    );
    add_image_sizes($sizes);
    add_filter('jpeg_quality', create_function('', 'return 98;'));
?>