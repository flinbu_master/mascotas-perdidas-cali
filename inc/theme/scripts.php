<?php
    global $theme_location;
    $css = $theme_location . '/assets/css';
    $js = $theme_location . '/assets/js';
    $plugins = $theme_location . '/assets/plugins';

    $scripts = array(
        //Custom JS & CSS
        array(
            'name' => 'mascotas-scripts',
            'type' => 'script',
            'location' => $js . '/mascotas.min.js',
            'deps' => array('jquery', 'bootstrap-scripts'),
            'footer' => true
        ),
        array(
            'name' => 'mascotas-scripts-dev',
            'type' => 'script',
            'location' => $js . '/mascotas.js',
            'deps' => array('jquery', 'bootstrap-scripts'),
            'footer' => true
        ),
        array(
            'name' => 'mascotas-styles',
            'type' => 'style',
            'location' => $css . '/mascotas.css',
            'deps' => array('lato', 'material-icons', 'font-awesome')
        ),
        array(
            'name' => 'mascotas-styles-dev',
            'type' => 'style',
            'location' => $css . '/mascotas.min.css',
            'deps' => array('lato', 'material-icons', 'font-awesome')
        ),
        //Font Lato
        array(
            'name' => 'lato',
            'type' => 'style',
            'location' => 'http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic'
        ),
        //Material Icons
        array(
            'name' => 'material-icons',
            'type' => 'style',
            'location' => $css . '/material-icons.css'
        ),
        //Font Awesome
        array(
            'name' => 'font-awesome',
            'type' => 'style',
            'location' => $css . '/font-awesome.css'
        ),
        //Bootstrap
        array(
            'name' => 'bootstrap-scripts',
            'type' => 'script',
            'location' => $plugins . '/bootstrap/bootstrap.min.js',
            'deps' => array('jquery'),
            'footer' => true
        ),
        //Owl Carousel
        array(
            'name' => 'owl-carousel-scripts',
            'type' => 'script',
            'location' => $plugins . '/owl-carousel/owl.carousel.min.js',
            'deps' => array('jquery'),
            'footer' => true
        ),
        array(
            'name' => 'owl-carousel-styles',
            'type' => 'style',
            'location' => $plugins . '/owl-carousel/owl.carousel.min.css'
        ),
        //WOW
        array(
            'name' => 'wow-scripts',
            'type' => 'script',
            'location' => $plugins . '/wow/wow.min.js',
            'footer' => true
        ),
        array(
            'name' => 'wow-styles',
            'type' => 'style',
            'location' => $plugins . '/wow/wow.animate.min.css'
        ),
        //GMAP 3
        array(
            'name' => 'google-maps',
            'type' => 'script',
            'location' => 'http://maps.google.com/maps/api/js?sensor=false&amp;language=en',
            'footer' => true
        ),
        array(
            'name' => 'gmap3',
            'type' => 'script',
            'location' => $plugins . '/gmap3/gmap3.min.js',
            'deps' => array('jquery', 'google-maps'),
            'footer' => true
        ),
        //Nivo Slider
        array(
            'name' => 'nivo-slider-scripts',
            'type' => 'script',
            'location' => $plugins . '/nivo-slider/jquery.nivo.slider.pack.js',
            'deps' => array('jquery'),
            'footer' => true
        ),
        array(
            'name' => 'nivo-slider-styles',
            'type' => 'style',
            'location' => $plugins . '/nivo-slider/nivo-slider.css'
        )
    );
    
    register_this($scripts);
?>