<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" data-toggle="collapse" data-target="#main-menu" aria-expanded="false" class="navbar-toggle collapsed">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="<?php echo get_permalink(get_theme_option('club_page')); ?>" class="navbar-brand">
                <?=stylize_this_title(get_the_title(get_theme_option( 'club_page')));?>
            </a>
        </div>
        <div class="collapse navbar-collapse" id="main-menu">
            <?php 
                wp_nav_menu(array( 
                    'menu' => 'club_menu',
                    'theme_location'=> 'club_menu', 
                    'depth' => 2,
                    'container' => false,
                    'menu_class' => 'nav navbar-nav',
                    'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
                    'walker' => new wp_bootstrap_navwalker()
                )); 
                wp_nav_menu(array(
                    'menu' => 'club_menu_right',
                    'theme_location' => 'club_menu_right',
                    'depth' => 2,
                    'container' => false,
                    'menu_class' => 'nav navbar-nav navbar-right',
                    'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
                    'walker' => new wp_bootstrap_navwalker()
                ));
            ?>
        </div>
    </div>
</nav>