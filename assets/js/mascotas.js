$ = jQuery.noConflict();

$(document).ready(function(){
    //Club Slider
    resize_club_slider();
    $(window).on('resize', function(){
       resize_club_slider(); 
    });
    var slider = $('section.club-slider .owl-carousel').owlCarousel({
        items : 1,
        loop : true,
        nav : false,
        dots : false,
        autoplay : true,
        animateOut : 'fadeOut',
        animateIn : 'fadeIn',
        autoplayTimeout : 6000,
        autoplaySpeed : 2000
    });
});

function resize_club_slider(){
    var slider = $('section.club-slider .owl-carousel'),
        win_width = $(window).width();
    
    slider.find('.slide').each(function(){
        var parent_height = $(this).parent().parent().height();
        $(this).width(win_width).height(parent_height);
    });
    
    slider.css({
        left : (win_width / 2) - (slider.width() / 2) + 'px' 
    });
}