<?php
    function enqueue_scripts(){
        if(get_theme_option('dev_mode')){
            $dev_css = 'mascotas-styles-dev';
            $dev_js = 'mascotas-scripts-dev';
        } else {
            $dev_css = 'mascotas-styles';
            $dev_js = 'mascotas-scripts';
        }
        $scripts = array(
            array(
                'name' => 'lato',
                'type' => 'style'
            ),
            array(
                'name' => 'material-icons',
                'type' => 'style'
            ),
            array(
                'name' => 'font-awesome',
                'type' => 'style'
            ),
            array(
                'name' => 'wow-styles',
                'type' => 'style'
            ),
            array(
                'name' => 'owl-carousel-styles',
                'type' => 'style'
            ),
            array(
                'name' => 'nivo-slider-styles',
                'type' => 'style'
            ),
            array(
                'name' => $dev_css,
                'type' => 'style'
            ),
            array(
                'name' => 'jquery',
                'type' => 'script'
            ),
            array(
                'name' => 'wow-scripts',
                'type' => 'script'
            ),
            array(
                'name' => 'owl-carousel-scripts',
                'type' => 'script'
            ),
            array(
                'name' => 'nivo-slider-scripts',
                'type' => 'script'
            ),
            array(
                'name' => 'google-maps',
                'type' => 'script'
            ),
            array(
                'name' => 'gmap3',
                'type' => 'script'
            ),
            array(
                'name' => 'bootstrap-scripts',
                'type' => 'script'
            ),
            array(
                'name' => $dev_js,
                'type' => 'script'
            )
        );
        enqueue_this($scripts);
        
        wp_localize_script($dev_js, 'mascotas_object', array(
            'ajax_url' => admin_url('admin-ajax.php'),
            'user_nonce' => wp_create_nonce('user_nonce'),
            'pet_nonce' => wp_create_nonce('pet_nonce'),
            'search_nonce' => wp_create_nonce('search_nonce'),
            'alert_nonce' => wp_create_nonce('alert_nonce'),
            'home_url' => get_bloginfo('home'),
            'club_url' => get_permalink(get_theme_option('club_page')),
            'current_url' => get_permalink(get_the_ID())
        ));
    }
    add_action('wp_enqueue_scripts', 'enqueue_scripts');

	function state_admin_bar(){
        return false;
	}
	add_filter('show_admin_bar', 'state_admin_bar');

    function footer_third_scripts(){
        theme_third_scripts('footer_scripts');
    }
    add_action('wp_footer', 'footer_third_scripts');

    function header_third_scripts(){
        theme_third_scripts('header_scripts');
    }
    add_action('wp_head', 'header_third_scripts');

    remove_action('wp_head', 'wp_generator');
    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'wlwmanifest_link');
    remove_action('wp_head', 'wp_shortlink_wp_head');
?>