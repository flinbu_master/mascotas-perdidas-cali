<?php
    global $theme_slug;


    register_sidebar(array(
        'name' => 'Default Sidebar',
        'id' => 'default_sidebar',
        'description' => 'Default sidebar',
        'class' => 'default_sidebar',
        'before_widget' => '<li id="%1$s" class="widget col-xs-12 %2$s">',
        'after_widget' => '</li>',
        'before_title' => '<h3 class="title">',
        'after_title' => '</h3>'
    ));	

    $sidebars = get_theme_option('sidebars');

    if($sidebars){
        foreach($sidebars as $sidebar) {
            $name = $sidebar['sidebar_name'];
            $slug = sanitize_title($name);
            $description = $sidebar['sidebar_description'];

            register_sidebar(array(
                'name' => $name,
                'id' => $theme_slug . $slug,
                'description' => $description,
                'class' => $slug,
                'before_widget' => '<li id="%1$s" class="widget col-xs-12 %2$s">',
                'after_widget' => '</li>',
                'before_title' => '<h3 class="title">',
                'after_title' => '</h3>'
            ));		
        }
    }
?>