<?php
    $features = array(
        array('post-formats', array('video', 'gallery')),
        array('post-thumbnails'),
        array('html5', array('comment-list', 'comment-form', 'search-form', 'gallery', 'caption', 'widgets')),
        array('title-tag')
    );
    support_this($features);
?>