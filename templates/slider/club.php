<?php
    $slider = get_field('home_slider');
    $slides = get_field('images', $slider);
    $logo = array(
        '1x' => wp_get_attachment_image_src(get_field('logo'), 'full'),
        '2x' => wp_get_attachment_image_src(get_field('logo_retina'), 'full')
    );
?>
<section class="club-slider">
   <div class="container">
       <div class="intro col-xs-13 col-md-6 col-md-offset-6">
           <div class="logo col-xs-12">
               <img src="<?=$logo['1x'][0];?>" alt="<?php the_title(); echo ' - '; bloginfo('name'); ?>" class="img-responsive" />
               <img src="<?=$logo['2x'][0];?>" alt="<?php the_title(); echo ' - '; bloginfo('name'); ?>" class="img-responsive retina" />
           </div>
           <div class="form col-xs-12">
               <h3><?=get_field('form_title');?></h3>
               <p><?=get_field('form_description');?></p>
               <input type="text" id="code_search" placeholder="Código" />
               <button type="button" data-code="code-search"><?=get_field('button_label');?></button>
           </div>
       </div>
   </div>
    <div class="owl-carousel">
        <?php foreach($slides as $slide) : ?>
            <div style="background-image: url(<?=$slide['sizes']['header-slider'];?>);" class="slide"></div>
        <?php endforeach; ?>
    </div>
    <div class="overlay"></div>
</section>