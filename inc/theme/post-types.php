<?php
    function custom_types(){
        $post_types = array(
            array(
                'name' => 'slider',
                'general_name' => 'Sliders',
                'singular_name' => 'slider',
                'sufix_min' => 'o',
                'gen_conector' => 'los',
                'level' => 0,
                'exclude_from_search' => true,
                'supports' => array(
                    'title',
                    'thumbnail'
                ),
                'icon' => 'dashicons-slides',
                'has_fields' => true,
                'has_tax' => false,
                'has_front' => false,
                'rewrite' => false
            ),
            array(
                'name' => 'mascotas',
                'general_name' => 'Mascotas',
                'singular_name' => 'Mascota',
                'sufix_min' => 'a',
                'gen_conector' => 'las',
                'level' => 0,
                'exclude_from_search' => false,
                'supports' => array(
                    'title',
                    'thumbnail',
                    'editor',
                    'comments',
                    'excerpt'
                ),
                'icon' => 'dashicons-heart',
                'has_fields' => true,
                'has_tax' => true,
                'has_front' => true,
                'rewrite' => false
            ),
            array(
                'name' => 'alerts',
                'general_name' => 'Alertas',
                'singular_name' => 'Alerta',
                'sufix_min' => 'a',
                'gen_conector' => 'las',
                'level' => 0,
                'exclude_from_search' => true,
                'supports' => array(
                    'title',
                    'editor'
                ),
                'icon' => 'dashicons-format-chat',
                'has_fields' => false,
                'has_tax' => false,
                'has_front' => false,
                'rewrite' => false
            ),
            array(
                'name' => 'casos',
                'general_name' => 'Casos',
                'singular_name' => 'Caso',
                'sufix_min' => 'o',
                'gen_conector' => 'los',
                'level' => 0,
                'exclude_from_search' => false,
                'supports' => array(
                    'title',
                    'editor',
                    'thumbnail',
                    'comments',
                    'excerpt'
                ),
                'icon' => 'dashicons-megaphone',
                'has_fields' => true,
                'has_tax' => true,
                'has_front' => true,
                'rewrite' => false
            )
        );
        register_custom_post_types($post_types);
    }
    add_action('init', 'custom_types');

    function get_pre_post($query){
        if(!$query->is_main_query())
            return;
        
        if( 2 != count($query->query) || !isset($query->query['page'])){
            return;
        }
        
        if(!empty($query->query['name'])){
            $query->set('post_type', array('post', 'page', 'casos', 'alerts', 'mascotas'));
        }
    }
    add_action('pre_get_posts', 'get_pre_post');
?>